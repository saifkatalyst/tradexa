import { Component, OnInit } from '@angular/core';
import $ from "jquery";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {


    $(".default_option").click(function () {
      $(".dropdown ul").addClass("active");
    });

    $(".dropdown ul li").click(function () {
      var text = $(this).text();
      $(".default_option").text(text);
      $(".dropdown ul").removeClass("active");
    });
  }

}
